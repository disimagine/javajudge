import java.util.Arrays;
import java.util.StringTokenizer;

public class SentenceProcessor {
	public String replaceWord(String target, String replacement, String sentense){
  		// sentense=sentense.replaceAll("\\s"+target," "+replacement);
		String[] cmds = sentense.split(" ");
		for (int i=0; i < cmds.length; i++){
			if (target.equals(cmds[i])){
				cmds[i] = replacement;
			}
		}
  		
		return Arrays.toString(cmds).replace(",", "").replace("[", "").replace("]", "");
//		String replaceSample = "String replacer example with replaceFirst";
//		String newString = replaceSample.replaceFirst("re","RE");
//		output result: String REplacer example of REplacing Character Sequence
//									 ^ ^  separated "re" won't be recognized

  	}
	public String removeDuplicatedWords(String inputText){
		StringTokenizer a=new StringTokenizer(inputText," ,",true);
		String[] x=new String[a.countTokens()];
		int tokenNum=0;
		tokenNum=a.countTokens();
		//System.out.println(tokenNum);
		for(int i=0;i<tokenNum;i++){
			//System.out.println(a.countTokens());
			x[i]=a.nextToken();
			//System.out.println("x[%d]="+x[i]);
		}
		for(int i=0;i<tokenNum;i++){
			int j=i+1;
			while(x[i]!=" "&&j<=tokenNum-1){
				//System.out.println("here");
				if(!(x[i].equals(","))&&x[i].equals(x[j])){
					//System.out.println("yes");
					x[j]=" ";
					j++;

				}
				else
					//System.out.println("no");
					j++;
					
			}
		}
		
		String result = x[0];
		//System.out.println(result);
		for(int i=1;i<tokenNum;i++){
			//if((x[i].equals(" ")))
			//	continue;
			//if (x[i]!=" "){
			//	result=result+x[i]+" ";
			//}
			
			if (!(x[i].equals(" "))){
				if((x[i].equals(","))){
					result=result+x[i];
				}
				else	
					result=result+" "+x[i];
			}

			//System.out.println(result);
		}
		//if (!(x[tokenNum-1].equals(" ")))
		//	result=result+x[tokenNum-1];

		//System.out.println(result);
		return result;
	}
	// TODO: any other methods are optional here.
}
