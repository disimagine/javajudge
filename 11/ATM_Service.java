


public class ATM_Service {
	public boolean checkBalance(Account account, int money) throws ATM_Exception{
		if (account.getBalance() < money){
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH); 
		}
		return true;
	}
	public boolean isValidAmount(int money) throws ATM_Exception{
		if (money % 1000 != 0){
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID); 
		}
//		System.out.println("here 16");
		return true;
	}
	public void withdraw(Account account, int money){
		try{
			if(checkBalance(account, money) && isValidAmount(money)){
				account.setBalance(account.getBalance()-money);
//				System.out.println("minus");
			}
			

		}catch(ATM_Exception e){
			System.out.println(e.getMessage());
		
		}finally{
			// TODO
			System.out.println("updated balance : "+ account.getBalance());///??????
		}
		return;
	}
}
