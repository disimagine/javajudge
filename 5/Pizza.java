public class Pizza {
	private String size;
	private int numOfCheese;
	private int numOfPepperoni;
	private int numOfHam;
	
	public Pizza(){
		this.size="small";
		this.numOfCheese=1;
		this.numOfPepperoni=1;
		this.numOfHam=1;
	}
	
	public Pizza(String size, int numOfCheese, int numOfPepperoni, int numOfHam){
		this.size=size;
		this.numOfCheese=numOfCheese;
		this.numOfPepperoni=numOfPepperoni;
		this.numOfHam=numOfHam;
	}
	public String getSize() {
		return (new String(size));
	}

	public void setSize(String size) {
		this.size=size;
	}

	public int getNumberOfCheese() {
		return numOfCheese;
	}

	public void setNumberOfCheese(int numOfCheese) {
		this.numOfCheese=numOfCheese;
	}

	public int getNumberOfPepperoni() {
		return numOfPepperoni;
	}

	public void setNumberOfPepperoni(int numOfPepperoni) {
		this.numOfPepperoni=numOfPepperoni;
	}

	public int getNumberOfHam() {
		return numOfHam;
	}

	public void setNumberOfHam(int numOfHam) {
		this.numOfHam=numOfHam;
	}
	
	public double calcCost(){
		int sizeCost=0;
		switch(size){
		case "small":
			sizeCost=10;break;
		case "medium":
			sizeCost=12;break;
		case "large":
			sizeCost=14;break;
		}
		return (sizeCost+2*(numOfCheese+numOfPepperoni+numOfHam));
	}

	public boolean equals(Pizza pizza){
		if(this.size.equals(pizza.size)&&
				this.numOfCheese==pizza.numOfCheese&&
				this.numOfPepperoni==numOfPepperoni&&
				this.numOfHam==numOfHam){
			return true;
		}
		else
			return false;
	}

	@Override
	public String toString() {
		return ("size = "+size+", numOfCheese = "+numOfCheese
				+", numOfPepperoni = "+numOfPepperoni+", numOfHam = "+numOfHam);
	}
}
