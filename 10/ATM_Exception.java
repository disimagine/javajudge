

public class ATM_Exception extends Exception {

	private static final long serialVersionUID = 1L;
	
	public static enum ExceptionTYPE{
		BALANCE_NOT_ENOUGH,
		AMOUNT_INVALID
	}
	
	private ExceptionTYPE exceptionCondition = null;
	
	public ATM_Exception(ExceptionTYPE type){
		exceptionCondition = type;
	}
	
	public String getMessage(){
		switch(this.exceptionCondition){
			case BALANCE_NOT_ENOUGH:
				return new String("BALANCE_NOT_ENOUGH");
			case AMOUNT_INVALID:
				return new String("AMOUNT_INVALID");
			default:
				return new String("All is well!");
				
		}
		
	}

}
