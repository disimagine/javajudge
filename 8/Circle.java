public class Circle extends Shape {
	
	public Circle(double length) {
		super(length);//----(1)
		//this.length=length;----(2)
	}
	//code (2) instead of (1):ERROR
	//If a derived class constructor does not include an invocation of super, 
	//then the no-argument constructor of the base class will automatically be invoked
	//This can result in an error if the base class has not defined a no-argument constructor

	@Override
	public void setLength(double length) {
		this.length=length;
	}

	@Override
	public double getArea() {
		return Math.PI*(length/2.0)*(length/2.0);
	}

	@Override
	public double getPerimeter() {
		return length*Math.PI;
	}

	
}
