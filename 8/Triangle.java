public class Triangle extends Shape {
	public Triangle(double length) {
		super(length);
	}

	public void setLength(double length){
		this.length=length;
	}

	@Override
	public double getArea() {
		return length*length*Math.sqrt(3)/4.0;
	}

	@Override
	public double getPerimeter() {
		return 3*length;
	}
}
