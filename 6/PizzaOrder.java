public class PizzaOrder {
	private int numPizzas;
	private Pizza pizza1;
	private Pizza pizza2;
	private Pizza pizza3;
	
	public boolean setNumberPizzas(int numPizzas){
		if (numPizzas<=3 && numPizzas>=1){
				this.numPizzas=numPizzas;
				return true;
		}
			return false;
	}

	public void setPizza1(Pizza pizza) {
		// pizza1.setSize=pizza.size;
		// pizza1.numOfCheese=pizza.getNumOfCheese();
		// pizza1.numOfPepperoni=pizza.getNumOfPepperoni();
		// pizza1.numOfHam=pizza.getNumOfHam();
		pizza1 = new Pizza(pizza.getSize(), pizza.getNumberOfCheese(), pizza.getNumberOfPepperoni(), pizza.getNumberOfHam());
	}

	public void setPizza2(Pizza pizza) {
		// pizza2.size=pizza.size;
		// pizza2.numOfCheese=pizza.getNumOfCheese();
		// pizza2.numOfPepperoni=pizza.getNumOfPepperoni();
		// pizza2.numOfHam=pizza.getNumOfHam();
		pizza2 = new Pizza(pizza.getSize(), pizza.getNumberOfCheese(), pizza.getNumberOfPepperoni(), pizza.getNumberOfHam());
	}

	public void setPizza3(Pizza pizza) { 
		// pizza3.size=pizza.size;
		// pizza3.numOfCheese=pizza.getNumOfCheese();
		// pizza3.numOfPepperoni=pizza.getNumOfPepperoni();
		// pizza3.numOfHam=pizza.getNumOfHam();
		pizza3 = new Pizza(pizza.getSize(), pizza.getNumberOfCheese(), pizza.getNumberOfPepperoni(), pizza.getNumberOfHam());
	}
	
	public double calcTotal(){
		double sum=0;
		switch(numPizzas){
		case 3:
			sum=sum+pizza3.calcCost();
		case 2:
			sum=sum+pizza2.calcCost();
		case 1:
			sum=sum+pizza1.calcCost();break;
		default:
			System.out.println("the number of pizza should be between 1 to 3.");
				
		}
		return sum;
	}
}

