public class GreenCrud {
	public int calPopulation(int initialSize, int day){
		/*
  		 * your code here
  		 * 
  		 */
		//initialSize: the value of 'a0'
		// a time period of five days
		int n=day/5;
		int a0=initialSize;
		int a1=initialSize;
		int a2=initialSize;
		if (n==1)
			return a2;
		else{
			for (int i=2;i<=n;i++){//the answer is 'an' (the n th  item)
				a2=a0+a1;
				a0=a1;
				a1=a2;
			}
			return a2;
		}
		
	}
}
