import java.security.UnrecoverableKeyException;
import java.text.DecimalFormat;

public class SimpleCalculator {
	public static int call = 0;
	private double result;
	public String[] op;
	// public String val;
	private double val;

	public void calResult(String cmd) throws UnknownCmdException {
		// op = cmd.split("[/s]{1,}");
		op = cmd.split(" ");
		// System.out.println("op[0]:" + op[0] + " op[1]:" + op[1]);
		// System.out.println(op[0].trim() == "");
		// System.out.println(op.length);
		// op[0] = op[0]; // java.lang.ArrayIndexOutOfBoundsException: 0
		// op[1] = op[1]; // java.lang.ArrayIndexOutOfBoundsException: 1
		if (op.length != 2 || "".equals(op[0]) || "".equals(op[1])) {
			// Prevent java.lang.ArrayIndexOutOfBoundsException
			// Unsolved problem: " + 1" is caught by this, but still not "+ 1 ".
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		}
		if ("r".equals(cmd.trim()) || "R".equals(cmd.trim())) {
			call = -1;
		}
//		else if (!cmd.matches("[+-/*//]{1}[/s]{0,}[/d]+[.]{0,1}[/d]+")) {
//			throw new UnknownCmdException("operator is an unknown operator and value is an unknown value");
//		} else if (((op[0] != "+" || op[1] != "-") || (op[1] != "*" || op[1] != "/"))
//				&& (!op[1].matches("[/d]+[.]{1}[/d]+"))) {
//			throw new UnknownCmdException("operator is an unknown operator and value is an unknown value");
//		}
		else if ((!op[0].matches("[+-/*//]{1}")) && (!op[1].matches("[0-9]+[.]{0,1}[0-9]*"))) {

			throw new UnknownCmdException(op[0] + " is an unknown operator and " + op[1] + " is an unknown value");
		}
//		else if ((op[0] != "+" || op[1] != "-") || (op[1] != "*" || op[1] != "/")) {
//			throw new UnknownCmdException("operator is an unknown operator");
//		}
		else if (!op[0].matches("[+-/*//]{1}")) {
			throw new UnknownCmdException(op[0] + " is an unknown operator");
		}
//		else if (!op[1].matches("[0-9]+[.]{0,1}[/d]*")) {
//			throw new UnknownCmdException("value is an unknown value");
//		}
		else if (!op[1].matches("[0-9]+[.]{0,1}[0-9]*")) {
			throw new UnknownCmdException(op[1] + " is an unknown value");
		} else if ("/".equals(op[0]) && Double.valueOf(op[1]) == 0) {
			throw new UnknownCmdException("Can not divide by 0");
		} else {
			val = Double.valueOf(op[1]);
			if ("+".equals(op[0])) {
				result = result + val;
			} else if ("-".equals(op[0])) {
				result = result - val;
			} else if ("*".equals(op[0])) {
				result = result * val;
			} else {
				result = result / val;
			}
		}

	}

	public String getMsg() {
		DecimalFormat df = new DecimalFormat("0.0#");

//		if (call == 0) {
//			call++;
//			return "Calculator is on. Result = " + result;
//		} else if (call == 1) {
//			call++;
//			return "Result operator value = result. New Result = " + result;
//		} else if (call > 1) {
//			return "Result operator value = result. Updated Result = " + result;
//		} else if (call < 0) {
//			// call = 0;
//			return "Final result = " + df.format(result);
//		}

		if (call == 0) {
			call++;
			return "Calculator is on. Result = " + result;
		} else if (call == 1) {
			call++;
			return "Result " + op[0] + " " + df.format(Double.parseDouble(op[1])) + " = " + df.format(result)
					+ ". New result = " + df.format(result);
		} else if (call > 1) {
			return "Result " + op[0] + " " + df.format(Double.parseDouble(op[1])) + " = " + df.format(result)
					+ ". Updated result = " + df.format(result);
		} else if (call < 0) {
			// call = 0;
			return "Final result = " + df.format(result);
		}
		return null;
	}

	public boolean endCalc(String cmd) {
		if ("r".equals(cmd.trim()) || "R".equals(cmd.trim())) {
			call = -1;
			return true;
		}
		return false;
	}
}