public class SimpleArrayList {
	private Integer[] array;
	private int size;
	// optional variables here
	
	public SimpleArrayList(){
		size=0;
		array=null;
	}
	
	public SimpleArrayList(int initialSize){
		size=initialSize;
		array=new Integer[initialSize];
		for (int i=0;i<array.length;i++){
			array[i]=new Integer(0);
		}
	}
	
	public void add(Integer i){
		size++;
		Integer[] arraytemp=array;
		array=new Integer[size];
		for (int x=0;x<size-1;x++){
			array[x]=arraytemp[x];
		}
		arraytemp=null;
		array[size-1]=i;		
	}
	
	public Integer get(int index){
		if (index>size-1) return null;
		if (array[index] == null) return null;
		return new Integer(array[index]);
	}
	
	public Integer set(int index, Integer element){
		if(index>size-1) return null;
		Integer arraytemp=array[index];
		array[index]=element;
		return (arraytemp);
	}
	
	public boolean remove(int index){
		if (array[index]==null)
				return false;
		size--;
		Integer[] arraytemp=array;
		array=new Integer[size];
		for(int i=0;i<index;i++){
			array[i]=arraytemp[i];
		}
		for(int i=index;i<size;i++){
			array[i]=arraytemp[i+1];
		}
		arraytemp=null;
		return true;
	}
	
	public void clear(){
		array=null;
		size=0;
	}
	
	public int size(){
		return size;
	}
	
	public boolean retainAll(SimpleArrayList l){
		int same=0;
		for (int i=0;i<this.size;i++){
			for(int j=0;j<l.size;j++){
				if (array[i].equals(l.array[j])){
					array[same]=array[i];
					same++;
					break;
				}
			}
		}

		if(same==size) 
			return false;
		Integer[] arraytemp=new Integer[same];
		for(int i=0;i<same;i++){
			arraytemp[i]=array[i];
		}
		array=arraytemp;
		size=same;
		return true;
	}
	// optional methods here
}

